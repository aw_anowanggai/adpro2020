package id.ac.ui.cs.advprog.tutorial2.observer.service;

import id.ac.ui.cs.advprog.tutorial2.observer.core.Adventurer;
import id.ac.ui.cs.advprog.tutorial2.observer.core.Quest;

import java.util.List;

public interface GuildService {
        void broadcastQuest(Quest quest);
        List<Adventurer> getAdventurers();
}
